package com.sample.test.SeleniumTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
public class SampleSeleniumTest {

	@Test()
	public void invokeGoogle() throws InterruptedException{
		
		WebDriver driver = new FirefoxDriver() ;
		driver.get("https://www.google.com");
		
		Thread.sleep(20000);
		System.out.println("Browser Invoked..Successfully");
		driver.findElement(By.xpath("//*[text()='Sign in']")).click() ;
	}
	
}

